<?php

namespace Pashynskyi\Localizable\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Trait Localizable
 * @package Localizable\Traits
 *
 * @method Builder withLocalization()
 * @method Builder whereHasLocalization()
 * @method Model __first(Builder $query, string $fieldName, string $fieldValue, boolean $default = false)
 * @method Model __firstOrFail(Builder $query, string $fieldName, string $fieldValue)
 * @method Model __firstOrNew(Builder $query, string $fieldName, string $fieldValue)
 */
trait Localizable
{
    public function localization()
    {
        return $this->morphMany(static::getLocalizationClass(), 'localizable');
    }

    public static function scope__first(Builder $query, $fieldName, $fieldValue, $default = false)
    {
        if (\LaravelLocalization::getDefaultLocale() == \LaravelLocalization::getCurrentLocale()) {
            return $query->where($fieldName, $fieldValue)->first();
        } else if ($id = static::getIdFromLocalize($fieldName, $fieldValue)->first()) {
            return $query->find($id);
        }
        else {
            return $query->where($fieldName, $fieldValue)->first();
        }
        return false;
    }

    public static function scope__firstOrFail(Builder $query, $fieldName, $fieldValue)
    {
        $model = $query->__first($fieldName, $fieldValue);

        if (!$model || $model instanceof Builder) {
            abort(404);
        }

        return $model;
    }

    public static function scope__firstOrNew(Builder $query, $fieldName, $fieldValue)
    {
        $model = $query->__first($fieldName, $fieldValue);

        if (!$model || $model instanceof Builder) {
            return new static();
        }

        return $model;
    }

    public function __($field)
    {
        if (\LaravelLocalization::getDefaultLocale() == \LaravelLocalization::getCurrentLocale()) {
            return $this->{$field};
        }

        $localizationCollection = $this->localization;
        $index = $localizationCollection->search(function ($item) use ($field) {
            if ($item->field == $field && $item->language == app()->getLocale()) {
                return $item;
            }
        });

        $row = null;
        if ($index !== false && isset($localizationCollection[$index])) {
            $row = $localizationCollection[$index];
        }

        return $row && isset($row->value) ? $row->value : $this->{$field};
    }

    public function scopeWithLocalization(Builder $query)
    {
        return $query->with(['localization' => function ($query) {
            $query->where('language', app()->getLocale());
        }]);
    }

    public function scopeWhereHasLocalization(Builder $query)
    {
        if (\LaravelLocalization::getDefaultLocale() == \LaravelLocalization::getCurrentLocale()) {
            return $query;
        }

        return $query->whereHas('localization', function ($query) {
            $query->where('language', app()->getLocale());
        });
    }

    public static function getIdFromLocalize($field, $value)
    {
        $localizationsClass = static::getLocalizationClass();

        return $localizationsClass::select('localizable_id')->whereHasMorph('localizable', [static::class], function ($query) use ($field, $value) {
            $query->where('value', $value)->where('field', $field);
        })->pluck('localizable_id');
    }
}
